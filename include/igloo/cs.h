/* Copyright (C) 2020       Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef _LIBIGLOO__CS_H_
#define _LIBIGLOO__CS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <igloo/types.h>

/* All igloo_cs_*() functions MAY depend locale settings */

igloo_error_t igloo_cs_replace(const char *src, char **dst) igloo_ATTR_F_WARN_UNUSED_RESULT;
igloo_error_t igloo_cs_skip_spaces(const char **str);

igloo_error_t igloo_cs_to_bool(const char *str, bool *res) igloo_ATTR_F_WARN_UNUSED_RESULT;
igloo_error_t igloo_cs_to_int(const char *str, int *res) igloo_ATTR_F_WARN_UNUSED_RESULT;
igloo_error_t igloo_cs_to_uint(const char *str, unsigned int *res) igloo_ATTR_F_WARN_UNUSED_RESULT;

igloo_error_t igloo_cs_to_lower(char *str);
igloo_error_t igloo_cs_to_upper(char *str);
igloo_error_t igloo_cs_to_lower_first(char *str);
igloo_error_t igloo_cs_to_upper_first(char *str);

igloo_error_t igloo_cs_to_hex(const void *in, char **res, size_t len) igloo_ATTR_F_WARN_UNUSED_RESULT;
#ifdef __cplusplus
}
#endif

#endif /* ! _LIBIGLOO__CS_H_ */
