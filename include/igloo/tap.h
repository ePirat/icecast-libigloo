/* Copyright (C) 2018-2021  Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef _LIBIGLOO__TAP_H_
#define _LIBIGLOO__TAP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <igloo/types.h>

igloo_error_t   igloo_tap_init(void);
igloo_error_t   igloo_tap_fin(void);

igloo_error_t   igloo_tap_test(const char *desc, bool res);
#define         igloo_tap_test_success(desc, got) igloo_tap_test_error((desc), igloo_ERROR_NONE, (got))
igloo_error_t   igloo_tap_test_error(const char *desc, igloo_error_t expected, igloo_error_t got);
igloo_error_t   igloo_tap_diagnostic(const char *line);
igloo_error_t   igloo_tap_bail_out(const char *reason);
bool            igloo_tap_can_continue(igloo_error_t *error);

#ifdef __cplusplus
}
#endif

#endif /* ! _LIBIGLOO__TAP_H_ */
