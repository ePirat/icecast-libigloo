/* Copyright (C) 2018       Marvin Scholz <epirat07@gmail.com>
 * Copyright (C) 2018-2020  Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef _LIBIGLOO__TYPES_H_
#define _LIBIGLOO__TYPES_H_

// this file contains common types for libigloo used in most modules.

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <inttypes.h>

#include <igloo/typedef.h>
#include <igloo/config.h>

/* used for .control_version-members, holding ABI version. */
typedef int_least64_t igloo_control_version_t igloo_ATTR_T_ALIGNED(8);
typedef int_least64_t igloo_control_type_t;

typedef struct {
    /* ABI version of this structure */
    igloo_control_version_t     control_version;
    /* Size of this control structure */
    size_t                      control_length;
    /* ABI version of the structure including this control structure */
    igloo_control_version_t     type_version;
    /* Size of the structure including this control structure */
    size_t                      type_length;
    /* Type ID of the structure including this control structure */
    igloo_control_type_t        type_type;
} igloo_control_t igloo_ATTR_T_ALIGNED(8);

/* ---[ error.[hc] ]--- */
#define igloo_PRIerror PRIiLEAST32
typedef int_least32_t igloo_error_t;

/* ---[ feature.[hc] ]--- */
typedef const struct igloo_feature_tag igloo_feature_t;

/* ---[ list.[hc] ]--- */
typedef struct igloo_list_tag igloo_list_t;

/* ---[ ro.[hc] ]--- */

typedef struct igloo_ro_stub_tag igloo_ro_stub_t;
typedef struct igloo_ro_tiny_tag igloo_ro_tiny_t;
typedef struct igloo_ro_full_tag igloo_ro_full_t;
typedef struct igloo_ro_object_group_tag igloo_ro_object_group_t;

igloo_RO_FORWARD_OPEN_TYPE(igloo_ro_tiny_t);
igloo_RO_FORWARD_OPEN_TYPE(igloo_ro_full_t);
igloo_RO_FORWARD_TYPE(igloo_ro_object_group_t);
igloo_RO_FORWARD_TYPE(igloo_ro_t);
igloo_RO_FORWARD_TYPE(igloo_ro_stub_t);

#ifdef igloo_ATTR_T_TRANSPARENT_UNION
typedef union {
    /* Those are libigloo's own types */
    igloo_RO_TYPE(igloo_ro_stub_t)
    igloo_RO_TYPE(igloo_ro_tiny_t)
    igloo_RO_TYPE(igloo_ro_full_t)
    igloo_RO_TYPE(igloo_ro_object_group_t)
    igloo_RO_TYPE(igloo_list_t)

    /* Now we add the current compilation unit's private types if any */
#ifdef igloo_RO_PRIVATETYPES
    igloo_RO_PRIVATETYPES
#endif

    /* Next are the application's types if any */
#ifdef igloo_RO_APPTYPES
    igloo_RO_APPTYPES
#endif

    /* And finnally all the types that are used by dependencies if any */
#ifdef igloo_RO_LIBTYPES
    igloo_RO_LIBTYPES
#endif
} igloo_ATTR_T_TRANSPARENT_UNION igloo_ro_t;
#else
typedef void * igloo_ro_t;
#endif

#ifdef __cplusplus
}
#endif

#endif /* ! _LIBIGLOO__TYPES_H_ */
