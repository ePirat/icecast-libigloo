/* Copyright (C) 2018-2021  Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <pthread.h>

#include <igloo/tap.h>
#include <igloo/error.h>

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

static size_t   test_count      = 0;
static bool     can_continue    = false;
static bool     started         = true;

#define begin() \
    pthread_mutex_lock(&mutex); \
    if (!started) { \
        pthread_mutex_unlock(&mutex); \
        return igloo_ERROR_BADSTATE; \
    } \
    (void)0

#define end() \
    pthread_mutex_unlock(&mutex); \
    return igloo_ERROR_NONE

igloo_error_t   igloo_tap_init(void)
{
    pthread_mutex_lock(&mutex);
    test_count = 0;
    can_continue = true;
    started = true;
    pthread_mutex_unlock(&mutex);

    return igloo_ERROR_NONE;
}

igloo_error_t   igloo_tap_fin(void)
{
    begin();

    printf("1..%zu\n", test_count);

    can_continue = false;
    started = false;

    end();
}

igloo_error_t   igloo_tap_test(const char *desc, bool res)
{
    const char *prefix = NULL;

    begin();

    if (!can_continue) {
        pthread_mutex_unlock(&mutex);
        return igloo_ERROR_NONE;
    }

    test_count++;

    if (res) {
        prefix = "ok";
    } else {
        prefix = "not ok";
    }

    if (desc) {
        printf("%s %zu %s\n", prefix, test_count, desc);
    } else {
        printf("%s %zu\n", prefix, test_count);
    }

    end();
}

igloo_error_t   igloo_tap_test_error(const char *desc, igloo_error_t expected, igloo_error_t got)
{
    // for now, maybe do something better later on.
    return igloo_tap_test(desc, got == expected);
}

igloo_error_t   igloo_tap_diagnostic(const char *line)
{
    begin();

    printf("# %s\n", line);

    end();
}

igloo_error_t   igloo_tap_bail_out(const char *reason)
{
    begin();

    can_continue = false;

    if (reason) {
        printf("Bail out! %s\n", reason);
    } else {
        printf("Bail out!\n");
    }

    end();
}

bool            igloo_tap_can_continue(igloo_error_t *error)
{
    bool ret = false;

    pthread_mutex_lock(&mutex);
    ret = started && can_continue;

    if (error) {
        if (started) {
            *error = igloo_ERROR_NONE;
        } else {
            *error = igloo_ERROR_BADSTATE;
        }
    }

    pthread_mutex_unlock(&mutex);

    return ret;
}

