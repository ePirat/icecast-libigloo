#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <igloo/tap.h>
#include <igloo/igloo.h>
#include <igloo/error.h>

int main(void)
{
    igloo_ro_t igloo_instance;

    igloo_tap_init();
    igloo_tap_test_success("igloo_initialize", igloo_initialize(&igloo_instance));

    // Ensure instance is valid
    igloo_tap_test_success("igloo_instance_validate", igloo_instance_validate(igloo_instance));

    // Ensure instance is not null
    igloo_tap_test("not null", !igloo_ro_is_null(igloo_instance));

    igloo_tap_test_success("unref", igloo_ro_unref(&igloo_instance));

    // Verify instance is null now
    igloo_tap_test("is null", igloo_ro_is_null(igloo_instance));

    igloo_tap_fin();
    return 0;
}
